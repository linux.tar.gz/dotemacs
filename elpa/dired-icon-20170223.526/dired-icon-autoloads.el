;;; dired-icon-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "dired-icon" "dired-icon.el" (23523 21846 675393
;;;;;;  417000))
;;; Generated autoloads from dired-icon.el

(autoload 'dired-icon-mode "dired-icon" "\
Display icons according to the file types in dired buffers.

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("dired-icon-pkg.el") (23523 21846 680638
;;;;;;  705000))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; dired-icon-autoloads.el ends here
