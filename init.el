(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
			("melpa" . "https://melpa.org/packages/")
			("marmalade" . "https://marmalade-repo.org/packages/")))
(package-initialize)

;; exec-path
(setq exec-path (append exec-path '("/usr/local/bin")))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(tool-bar-mode -1)
(toggle-frame-fullscreen)
(linum-mode)


(use-package which-key
  :ensure t
  :config
  (which-key-mode))

(use-package ivy
  :ensure t
  :diminish (ivy-mode)
  :bind (("C-x b" . ivy-switch-buffer))
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq ivy-count-format "%d/%d ")
  (setq ivy-dispaly-style 'fancy))

(use-package counsel
  :ensure t
  :bind
  (("M-y" . counsel-yank-pop)
   :map ivy-minibuffer-map
   ("M-y" . ivy-next-line)))

  

(use-package swiper
  :ensure t
  :bind (("C-s" . swiper)
	 ("C-r" . swiper)
	 ("C-c C-r" . ivy-resume)
	 ("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file))
  :config
  (progn
    (ivy-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq ivy-display-style 'fancy)
    (define-key read-expression-map (kbd "C-r") 'counsel-sxpression-history)
    ))

;; Company's Autocompletion
;; company itself
(use-package company
  :ensure t
  :config
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 3)
  (global-company-mode t))

;; backends
(use-package company-irony
  :ensure t
  :config
  (add-to-list 'company-backends 'company-iron))

;; the helper package
(use-package irony
  :ensure t
  :config
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))
;;eldoc ?


;; gotham theme
(use-package gotham-theme
  :ensure t
  :config
  (load-theme 'gotham t))


(use-package haskell-mode
  :ensure t
  :config
  (add-hook 'haskell-mode-hook 'turn-on-haskell-unicode-input-method))


;; Fira Code as Font
(set-frame-font "Fira Code Bold")
;; No Scroll Bars pls
(scroll-bar-mode -1)





;; Writing GNU Emacs Extensions
;; switch to next window, similar to C-x o
(global-set-key "\C-x\C-n" 'other-window)
;; switch to previous window with other-window -1

(defun other-window-backward (&optional n)
  "Switch to the previous window"
  (interactive "p")
  (other-window (- (if n n 1) )))
;; bind funciton to C-x C-p
(global-set-key "\C-x\C-p" 'other-window-backward)

;; Fortran mode
(add-to-list 'auto-mode-alist '("\\.f\\'" . f90-mode))

;; Tramp Mode setup
(setq tramp-default-method "scp")


(defun play-music()
  "play music "
  (interactive)
  (shell-command "/usr/local/bin/mplayer  /Users/mojo/007.mp3 &"))
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (haskell-mode gotham-theme company-irony company which-key counsel ivy use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (f90-mode dired-icon which-key use-package haskell-mode gotham-theme counsel company-irony))))
